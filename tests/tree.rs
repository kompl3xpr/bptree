use bptree::BpTreeMap;
use rand::prelude::*;

#[test]
pub fn general_test() {
    let n = 50000;
    let to_pair = |n| (n, n as f32 / 3.0);
    let mut contents: Vec<_> = (-n..n).map(to_pair).collect();
    contents.shuffle(&mut thread_rng());
    let mut tree = BpTreeMap::from_iter(contents.clone());
    let mut iter = tree.iter();
    for i in -n..n {
        assert_eq!(*iter.next().unwrap().0, i);
    }
    contents.shuffle(&mut thread_rng());
    for (k, v) in &contents {
        assert_eq!(tree.remove(k), Some((*k, *v)));
    }
    assert_eq!(tree.len(), 0);
    drop(BpTreeMap::from_iter(contents.into_iter()));
}

#[test]
pub fn empty_map() {
    struct NeverUsed;
    impl std::ops::Drop for NeverUsed {
        fn drop(&mut self) {
            unreachable!()
        }
    }
    drop(BpTreeMap::<NeverUsed, NeverUsed>::new());
    drop(BpTreeMap::<NeverUsed, NeverUsed>::new().into_iter())
}

#[test]
pub fn pop_first() {
    let n = 50000;
    let to_pair = |n| (n, ());
    let mut contexts: Vec<_> = (0..n).map(to_pair).collect();
    contexts.shuffle(&mut thread_rng());
    let mut tree = BpTreeMap::from_iter(contexts.clone());
    for i in 0..n {
        assert_eq!(tree.pop_first(), Some((i, ())));
    }
    assert!(tree.is_empty());
}

#[test]
pub fn pop_last() {
    let n = 50000;
    let to_pair = |n| (n, ());
    let mut contexts: Vec<_> = (0..n).map(to_pair).collect();
    contexts.shuffle(&mut thread_rng());
    let mut tree = BpTreeMap::from_iter(contexts.clone());
    for i in (0..n).rev() {
        assert_eq!(tree.pop_last_ologn(), Some((i, ())));
    }
    assert!(tree.is_empty());
}
