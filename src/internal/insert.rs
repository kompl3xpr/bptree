use crate::{constant::*, node::*};
use std::ptr::NonNull;

impl<K, V> InternalNode<K, V> {
    unsafe fn insert_and_split<T: FromAnyNode<K, V>>(
        &mut self,
        index: usize,
        key: KeyRef<K>,
        child: AnyNode<K, V>,
    ) -> (KeyRef<K>, NonNull<Self>) {
        use std::cmp::Ordering::*;
        let mut pself = NonNull::from(self);
        let this = pself.as_mut();
        let (take_idx, ins_idx) = match index.cmp(&((CAPACITY + 1) / 2)) {
            Greater => ((CAPACITY + 1) / 2, index - 1),
            Less => ((CAPACITY - 1) / 2, index),
            Equal => (B, 0),
        };
        let (mid_key, mid_child) = match take_idx == B {
            true => (key, child),
            false => {
                let ret = this.remove_unchecked(take_idx);
                this.insert_unchecked(ins_idx, key, child);
                ret
            }
        };

        let mut other = Self::new_ptr_with_child(mid_child);
        for _ in 0..HALF_CAP {
            let (k, c) = this.pop_unchecked();
            other.as_mut().insert_unchecked(0, k, c);
        }
        this.update_childrens_nth_from_index::<T>(ins_idx);
        other.as_mut().update_childrens_nth_from_index::<T>(0);
        Self::update_childrens_parent_from_index::<T>(other, 0);
        (mid_key, other)
    }

    pub fn insert<T: FromAnyNode<K, V>>(
        &mut self,
        idx: usize,
        key: KeyRef<K>,
        child: AnyNode<K, V>,
    ) {
        unsafe {
            let mut pself = NonNull::from(self);
            let this = pself.as_mut();
            if this.len < CAPACITY {
                this.insert_unchecked(idx, key, child);
                this.update_childrens_nth_from_index::<T>(idx);
                return;
            }
            let (mid, mut other) = this.insert_and_split::<T>(idx, key, child);
            let this = pself.as_mut();
            let (mut parent, idx) = match this.parent {
                None => {
                    let parent = InternalNode::new_ptr_with_child(this.any());
                    this.parent = Some(parent);
                    this.nth.write(0);
                    (parent, 0)
                }
                Some(parent) => (parent, this.nth.assume_init()),
            };
            other.as_mut().parent = Some(parent);
            parent
                .as_mut()
                .insert::<InternalNode<K, V>>(idx, mid, other.as_mut().any());
        }
    }
}
