use std::ptr::NonNull;

use crate::constant::*;
use crate::node::*;

impl<K, V> InternalNode<K, V> {
    unsafe fn merge_leaves(&mut self, index: usize) {
        let (_, mut right) = self.remove_unchecked(index);
        let mut left = *self.children().get_unchecked(index);
        let (l, r) = (left.leaf.as_mut(), right.leaf.as_mut());
        // println!("{:?}", (l.len(), r.len()));
        // assert!(l.len() + r.len() <= CAPACITY);
        l.next = r.next;
        let (keys, vals) = r.pairs();
        for i in 0..r.len() {
            let k = std::ptr::read(keys.get_unchecked(i) as *const _);
            let v = std::ptr::read(vals.get_unchecked(i) as *const _);
            l.push_unchecked(k, v);
        }
        r.set_len(0);
        self.update_childrens_nth_from_index::<LeafNode<K, V>>(index);
        drop(Box::from_raw(right.leaf.as_ptr()));
    }

    unsafe fn try_move_a_key_value_pair(&mut self, src: usize, dest: usize) -> bool {
        let cmp = src.cmp(&dest);
        let src = self.children_mut().get_unchecked_mut(src).leaf.as_mut();
        let dest = self.children_mut().get_unchecked_mut(dest).leaf.as_mut();
        if src.len() <= CAPACITY / 2 + 1 {
            return false;
        }
        use std::cmp::Ordering::*;
        match cmp {
            Less => {
                let (k, v) = src.pop_unchecked();
                dest.insert_unchecked(0, k, v);
            }
            Greater => {
                let (k, v) = src.remove_unchecked(0);
                dest.push_unchecked(k, v);
            }
            Equal => unreachable!(),
        };
        true
    }

    pub fn remove_leaf(mut pself: NonNull<Self>, index: usize) -> MaybeAnyNode<K, V> {
        unsafe {
            let this = pself.as_mut();
            let key_idx = index.saturating_sub(1);
            let other = if index == 0 { 1 } else { key_idx };
            if this.try_move_a_key_value_pair(other, index) {
                return MaybeAnyNode::none();
            }
            this.merge_leaves(key_idx);
            if this.len() >= CAPACITY / 2 {
                return MaybeAnyNode::none();
            }
            if let Some(parent) = this.parent() {
                let idx = this.nth().assume_init();
                return Self::remove_internal::<LeafNode<K, V>>(*parent, idx);
            }
            if this.len() == 0 {
                let mut some = *this.children().get_unchecked(0);
                *some.leaf.as_mut().parent_mut() = None;
                drop(Box::from_raw(pself.as_ptr()));
                return MaybeAnyNode { some };
            }
            MaybeAnyNode::none()
        }
    }
}

impl<K, V> InternalNode<K, V> {
    unsafe fn try_move_a_key_child_pair<T: FromAnyNode<K, V>>(
        &mut self,
        key_idx: usize,
        src: usize,
        dest: usize,
    ) -> bool {
        let cmp = src.cmp(&dest);
        let children = self.children_mut();
        let mut src = children.get_unchecked_mut(src).internal;
        let mut dest = children.get_unchecked_mut(dest).internal;
        if src.as_ref().len() <= (CAPACITY + 1) / 2 {
            return false;
        }
        use std::cmp::Ordering::*;
        let key_mut = self.keys_mut().get_unchecked_mut(key_idx);
        match cmp {
            Less => {
                let (k, c) = src.as_mut().pop_unchecked();
                let k = std::mem::replace(key_mut, k);
                *T::from_any(c).as_mut().parent_mut() = Some(dest);
                dest.as_mut().insert_first_unchecked(k, c);
                dest.as_mut().update_childrens_nth_from_index::<T>(0);
            }
            Greater => {
                let (k, c) = src.as_mut().take_first_unchecked();
                src.as_mut().update_childrens_nth_from_index::<T>(0);
                let k = std::mem::replace(key_mut, k);
                *T::from_any(c).as_mut().parent_mut() = Some(dest);
                *T::from_any(c).as_mut().nth_mut().assume_init_mut() = dest.as_ref().len() + 1;
                dest.as_mut().push_unchecked(k, c);
            }
            Equal => unreachable!(),
        };
        true
    }

    unsafe fn merge_internal<T: FromAnyNode<K, V>>(&mut self, index: usize) {
        let (mid, mut right) = self.remove_unchecked(index);
        let mut left = *self.children().get_unchecked(index);
        let (l, r) = (left.internal.as_mut(), right.internal.as_mut());
        let from_idx = l.len();
        l.push_unchecked(mid, *r.children().get_unchecked(0));
        let (keys, children) = (r.keys(), r.children());
        for i in 0..r.len() {
            let k = *keys.get_unchecked(i);
            let v = *children.get_unchecked(i + 1);
            l.push_unchecked(k, v);
        }
        l.update_childrens_nth_from_index::<T>(from_idx);
        Self::update_childrens_parent_from_index::<T>(left.internal, from_idx);
        self.update_childrens_nth_from_index::<InternalNode<K, V>>(index);
        drop(Box::from_raw(right.internal.as_ptr()));
    }

    pub fn remove_internal<T: FromAnyNode<K, V>>(
        mut pself: NonNull<Self>,
        index: usize,
    ) -> MaybeAnyNode<K, V> {
        unsafe {
            let this = pself.as_mut();
            let key_idx = index.saturating_sub(1);
            let other = if index == 0 { 1 } else { key_idx };
            if this.try_move_a_key_child_pair::<T>(key_idx, other, index) {
                return MaybeAnyNode::none();
            }
            this.merge_internal::<T>(key_idx);
            if this.len() >= CAPACITY / 2 {
                return MaybeAnyNode::none();
            }
            if let Some(parent) = this.parent() {
                let idx = this.nth().assume_init();
                return Self::remove_internal::<InternalNode<K, V>>(*parent, idx);
            }
            if this.len() == 0 {
                let mut some = *this.children().get_unchecked(0);
                *some.internal.as_mut().parent_mut() = None;
                drop(Box::from_raw(pself.as_ptr()));
                return MaybeAnyNode { some };
            }
            MaybeAnyNode::none()
        }
    }
}
