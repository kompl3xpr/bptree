mod constant {
    /// `B` should be greater than 2.
    pub(crate) const B: usize = 6;
    pub(crate) const CAPACITY: usize = B - 1;
    pub(crate) const HALF_CAP: usize = CAPACITY / 2;
}

mod utils;

mod internal;
mod leaf;
mod node;

mod tree;
pub use tree::{BpTreeMap, BpTreeSet};
