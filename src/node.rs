pub(crate) use crate::internal::{InternalNode, KeyRef};
pub(crate) use crate::leaf::LeafNode;
use std::mem::MaybeUninit;
use std::ptr::NonNull;

#[derive(Eq)]
pub(crate) union AnyNode<K, V> {
    pub leaf: NonNull<LeafNode<K, V>>,
    pub internal: NonNull<InternalNode<K, V>>,
}

impl<K, V> Clone for AnyNode<K, V> {
    fn clone(&self) -> Self {
        *self
    }
}
impl<K, V> Copy for AnyNode<K, V> {}
impl<K, V> std::cmp::PartialEq for AnyNode<K, V> {
    fn eq(&self, other: &Self) -> bool {
        unsafe { self.leaf == other.leaf }
    }
}

pub(crate) union MaybeAnyNode<K, V> {
    pub some: AnyNode<K, V>,
    pub none: *mut u8,
}

impl<K, V> MaybeAnyNode<K, V> {
    pub fn none() -> Self {
        Self {
            none: std::ptr::null_mut(),
        }
    }

    pub fn is_some(&self) -> bool {
        std::ptr::null_mut() != unsafe { self.none }
    }
}

impl<K, V> Clone for MaybeAnyNode<K, V> {
    fn clone(&self) -> Self {
        *self
    }
}
impl<K, V> Copy for MaybeAnyNode<K, V> {}

pub(crate) trait TreeNode<K, V> {
    fn parent(&self) -> &Option<NonNull<InternalNode<K, V>>>;
    fn parent_mut(&mut self) -> &mut Option<NonNull<InternalNode<K, V>>>;
    fn nth(&self) -> &MaybeUninit<usize>;
    fn nth_mut(&mut self) -> &mut MaybeUninit<usize>;
    fn any(&mut self) -> AnyNode<K, V>;
}

pub(crate) trait FromAnyNode<K, V>: TreeNode<K, V> {
    unsafe fn from_any(any: AnyNode<K, V>) -> NonNull<Self>;
}
