use crate::node::*;
use std::{marker::PhantomData, ptr::NonNull};

mod iter;
use iter::*;
mod iter_owned;
use iter_owned::*;
mod set;
pub use set::BpTreeSet;

pub struct BpTreeMap<K, V> {
    depth: usize,
    len: usize,
    root: AnyNode<K, V>,
    head_leaf: NonNull<LeafNode<K, V>>,
    _marker: PhantomData<(K, V)>,
}

impl<K: Ord, V> BpTreeMap<K, V> {
    fn get_leaf(&'_ self, key: &K) -> &'_ LeafNode<K, V> {
        unsafe { self.get_leaf_ptr(key).as_ref() }
    }

    fn get_leaf_mut(&'_ mut self, key: &K) -> &'_ mut LeafNode<K, V> {
        unsafe { self.get_leaf_ptr(key).as_mut() }
    }

    unsafe fn get_leaf_ptr(&'_ self, key: &K) -> NonNull<LeafNode<K, V>> {
        unsafe fn helper<K: Ord, V>(r: AnyNode<K, V>, k: &K, d: usize) -> NonNull<LeafNode<K, V>> {
            match d == 0 {
                true => r.leaf,
                _ => helper(r.internal.as_ref().get_child_by(k), k, d - 1),
            }
        }
        helper(self.root, key, self.depth)
    }
}

impl<K, V> BpTreeMap<K, V> {
    pub fn new() -> Self {
        let head_leaf = unsafe { LeafNode::new_ptr() };
        Self {
            depth: 0,
            len: 0,
            root: AnyNode { leaf: head_leaf },
            head_leaf,
            _marker: PhantomData,
        }
    }

    pub fn len(&self) -> usize {
        self.len
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

impl<K: Ord, V> BpTreeMap<K, V> {
    pub fn insert(&mut self, key: K, value: V) -> Option<V> {
        unsafe {
            let ret = self.get_leaf_mut(&key).insert(key, value);
            self.len += 1;
            let root: &dyn TreeNode<_, _> = match self.depth == 0 {
                true => self.root.leaf.as_ref(),
                false => self.root.internal.as_ref(),
            };
            if let Some(mut parent) = root.parent() {
                self.root = parent.as_mut().any();
                self.depth += 1;
            }
            ret
        }
    }

    pub fn remove(&mut self, key: &K) -> Option<(K, V)> {
        unsafe {
            let ((k, v), root) = LeafNode::remove(self.get_leaf_ptr(key), key)?;
            self.len -= 1;
            if root.is_some() {
                self.root = root.some;
                self.depth -= 1;
            }
            Some((k, v))
        }
    }

    pub fn get(&self, key: &K) -> Option<&V> {
        self.get_leaf(key).get_by(key)
    }

    pub fn get_mut(&mut self, key: &K) -> Option<&mut V> {
        self.get_leaf_mut(key).get_mut_by(key)
    }

    pub fn contains(&self, key: &K) -> bool {
        self.get_leaf(key).get_by(key).is_some()
    }

    pub fn pop_first(&mut self) -> Option<(K, V)> {
        let ((k, v), root) = LeafNode::remove_first(self.head_leaf)?;
        self.len -= 1;
        if root.is_some() {
            self.root = unsafe { root.some };
            self.depth -= 1;
        }
        Some((k, v))
    }

    pub fn pop_last_ologn(&mut self) -> Option<(K, V)> {
        unsafe fn helper<K: Ord, V>(r: AnyNode<K, V>, d: usize) -> NonNull<LeafNode<K, V>> {
            match d == 0 {
                true => r.leaf,
                _ => {
                    let internal = r.internal.as_ref();
                    let next = *internal.children().get_unchecked(internal.len());
                    helper(next, d - 1)
                }
            }
        }
        unsafe {
            let tail = helper(self.root, self.depth);
            let ((k, v), root) = LeafNode::remove_last(tail)?;
            self.len -= 1;
            if root.is_some() {
                self.root = root.some;
                self.depth -= 1;
            }
            Some((k, v))
        }
    }
}

impl<K, V> std::ops::Drop for BpTreeMap<K, V> {
    fn drop(&mut self) {
        unsafe fn helper<K, V>(r: AnyNode<K, V>, d: usize) {
            if d == 0 {
                drop(Box::from_raw(r.leaf.as_ptr()));
            } else {
                let internal = r.internal;
                let children = internal.as_ref().children();
                for child in children {
                    helper(*child, d - 1);
                }
                drop(Box::from_raw(internal.as_ptr()));
            }
        }
        unsafe { helper(self.root, self.depth) }
    }
}

use std::fmt::{Debug, Formatter as Fmt};

impl<K: Debug, V: Debug> Debug for BpTreeMap<K, V> {
    fn fmt(&self, f: &mut Fmt<'_>) -> std::fmt::Result {
        f.debug_map().entries(self.iter()).finish()
    }
}
