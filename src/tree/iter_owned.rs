use crate::{node::*, BpTreeMap};
use std::ptr::NonNull;

pub struct IterOwned<K, V> {
    current: Option<NonNull<LeafNode<K, V>>>,
    index: usize,
}

impl<K, V> Iterator for IterOwned<K, V> {
    type Item = (K, V);

    fn next(&mut self) -> Option<Self::Item> {
        self.current.and_then(|mut current| unsafe {
            if self.index < current.as_ref().len() {
                let (keys, vals) = current.as_mut().pairs();
                let k = std::ptr::read(&keys[self.index] as *const _);
                let v = std::ptr::read(&vals[self.index] as *const _);
                self.index += 1;
                return Some((k, v));
            }

            let next = current.as_ref().next();
            current.as_mut().set_len(0);
            drop(Box::from_raw(current.as_ptr()));
            self.index = 0;
            self.current = next;
            self.next()
        })
    }
}

impl<K, V> std::ops::Drop for IterOwned<K, V> {
    fn drop(&mut self) {
        while self.index > 0 {
            if self.next().is_none() {
                break;
            }
        }
        unsafe {
            let mut leaf_node = self.current;
            while let Some(leaf) = leaf_node {
                leaf_node = leaf.as_ref().next();
                drop(Box::from_raw(leaf.as_ptr()));
            }
        }
    }
}

impl<K, V> IntoIterator for BpTreeMap<K, V> {
    type Item = (K, V);

    type IntoIter = IterOwned<K, V>;

    fn into_iter(mut self) -> Self::IntoIter {
        unsafe fn drop_internal<K, V>(r: AnyNode<K, V>, d: usize) {
            if d > 0 {
                let internal = r.internal;
                let children = internal.as_ref().children();
                for child in children {
                    drop_internal(*child, d - 1);
                }
                drop(Box::from_raw(internal.as_ptr()));
            }
        }
        unsafe { drop_internal(self.root, self.depth) };
        self.root = unsafe { LeafNode::new_ptr().as_mut().any() };
        self.depth = 0;

        IterOwned {
            current: Some(self.head_leaf),
            index: 0,
        }
    }
}

impl<K: Ord, V> FromIterator<(K, V)> for BpTreeMap<K, V> {
    fn from_iter<T: IntoIterator<Item = (K, V)>>(iter: T) -> Self {
        let mut ret = BpTreeMap::new();
        for (k, v) in iter.into_iter() {
            ret.insert(k, v);
        }
        ret
    }
}

#[cfg(test)]
mod test {
    use std::{cell::RefCell, rc::Rc};

    use crate::BpTreeMap;
    struct Value(Rc<RefCell<usize>>);
    impl std::ops::Drop for Value {
        fn drop(&mut self) {
            *self.0.borrow_mut() += 1;
        }
    }

    #[test]
    fn drop_and_order_check() {
        let n = 1000;
        let counter = Rc::new(RefCell::new(0));
        let make_val = || Value(Rc::clone(&counter));

        let mut tree = BpTreeMap::new();
        (n * 3 / 4..n).rev().for_each(|i| {
            tree.insert(i, make_val());
        });
        (n / 2..n * 3 / 4).for_each(|i| {
            tree.insert(i, make_val());
        });
        (n / 4..n / 2).rev().for_each(|i| {
            tree.insert(i, make_val());
        });
        (0..n / 4).for_each(|i| {
            tree.insert(i, make_val());
        });

        let v = tree.into_iter().map(|(k, _)| k).collect::<Vec<_>>();
        assert_eq!(*counter.borrow(), n);
        assert_eq!(v, (0..n).collect::<Vec<_>>());
    }

    #[test]
    fn from_iter() {
        let mut tree = BpTreeMap::from_iter([(1, 'a'), (2, 'b'), (3, 'c')]);
        tree = tree.into_iter().collect();
        assert_eq!(tree.get(&2), Some(&'b'));
    }

    #[test]
    fn over_use() {
        let mut iter = BpTreeMap::from_iter((0..100).map(|a| (a, a))).into_iter();
        for i in 0..100 {
            assert_eq!(iter.next(), Some((i, i)));
        }
        assert_eq!(iter.next(), None);
        assert_eq!(iter.next(), None);
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn drop_check2() {
        let counter = Rc::new(RefCell::new(0));
        let make_val = || Value(Rc::clone(&counter));

        BpTreeMap::<i32, Value>::new().into_iter();
        let mut iter = BpTreeMap::from_iter((0..100).map(|a| (a, make_val()))).into_iter();
        for _ in 0..50 {
            iter.next();
        }
        drop(iter);
        assert_eq!(*counter.borrow(), 100);
    }
}
