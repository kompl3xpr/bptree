use crate::{node::*, BpTreeMap};
use std::{marker::PhantomData, ptr::NonNull};

impl<K, V> BpTreeMap<K, V> {
    pub fn iter(&self) -> Iter<'_, K, V> {
        Iter {
            current: Some(self.head_leaf),
            index: 0,
            _marker: PhantomData,
        }
    }

    pub fn iter_mut(&mut self) -> IterMut<'_, K, V> {
        IterMut {
            current: Some(self.head_leaf),
            index: 0,
            _marker: PhantomData,
        }
    }
}

impl<'a, K, V> IntoIterator for &'a BpTreeMap<K, V> {
    type Item = (&'a K, &'a V);
    type IntoIter = Iter<'a, K, V>;
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, K, V> IntoIterator for &'a mut BpTreeMap<K, V> {
    type Item = (&'a K, &'a mut V);
    type IntoIter = IterMut<'a, K, V>;
    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

pub struct Iter<'a, K, V> {
    current: Option<NonNull<LeafNode<K, V>>>,
    index: usize,
    _marker: PhantomData<&'a (K, V)>,
}

impl<'a, K, V> Iterator for Iter<'a, K, V> {
    type Item = (&'a K, &'a V);

    fn next(&mut self) -> Option<Self::Item> {
        self.current.and_then(|current| unsafe {
            match self.index < current.as_ref().len() {
                true => {
                    let (keys, vals) = current.as_ref().pairs();
                    let k = &keys[self.index];
                    let v = &vals[self.index];
                    self.index += 1;
                    Some((k, v))
                }
                false => {
                    self.current = current.as_ref().next();
                    self.index = 0;
                    self.next()
                }
            }
        })
    }
}

pub struct IterMut<'a, K, V> {
    current: Option<NonNull<LeafNode<K, V>>>,
    index: usize,
    _marker: PhantomData<&'a mut (K, V)>,
}

impl<'a, K, V> Iterator for IterMut<'a, K, V> {
    type Item = (&'a K, &'a mut V);

    fn next(&mut self) -> Option<Self::Item> {
        self.current.and_then(|mut current| unsafe {
            match self.index < current.as_ref().len() {
                true => {
                    let (keys, vals) = current.as_mut().pairs_mut();
                    let k = &keys[self.index];
                    let v = &mut vals[self.index];
                    self.index += 1;
                    Some((k, v))
                }
                false => {
                    self.current = current.as_ref().next();
                    self.index = 0;
                    self.next()
                }
            }
        })
    }
}

#[cfg(test)]
mod test {
    use crate::BpTreeMap;

    #[test]
    fn iter() {
        let tree = BpTreeMap::from_iter((0..100).map(|i| (i, ())));
        for (i, (k, _)) in tree.iter().enumerate() {
            assert_eq!(i, *k);
            if i == 50 {
                break;
            }
        }
    }

    #[test]
    fn iter_mut() {
        let mut tree = BpTreeMap::from_iter((0..100).map(|i| (i, i)));
        let iter_mut = tree.iter_mut();
        iter_mut.for_each(|(_, v)| *v += 1);
        for (i, (_, v)) in tree.iter().enumerate() {
            assert_eq!(i + 1, *v);
        }
    }
}
