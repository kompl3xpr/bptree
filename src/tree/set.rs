use super::BpTreeMap;

pub struct BpTreeSet<T> {
    inner: BpTreeMap<T, ()>,
}

impl<T> BpTreeSet<T> {
    #[inline]
    pub fn new() -> Self {
        let inner = BpTreeMap::new();
        Self { inner }
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.inner.len()
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.inner.is_empty()
    }
}

impl<T: Ord> BpTreeSet<T> {
    #[inline]
    pub fn insert(&mut self, elem: T) -> bool {
        self.inner.insert(elem, ()).is_none()
    }

    #[inline]
    pub fn remove(&mut self, elem: &T) -> Option<T> {
        self.inner.remove(elem).map(|(k, _)| k)
    }

    #[inline]
    pub fn contains(&self, elem: &T) -> bool {
        self.inner.contains(elem)
    }

    #[inline]
    pub fn pop_first(&mut self) -> Option<T> {
        self.inner.pop_first().map(|(t, _)| t)
    }

    #[inline]
    pub fn pop_last_ologn(&mut self) -> Option<T> {
        self.inner.pop_last_ologn().map(|(t, _)| t)
    }
}

impl<T> BpTreeSet<T> {
    #[inline]
    pub fn iter(&'_ self) -> Iter<'_, T> {
        let inner = self.inner.iter();
        Iter { inner }
    }
}

impl<T> From<BpTreeMap<T, ()>> for BpTreeSet<T> {
    #[inline]
    fn from(inner: BpTreeMap<T, ()>) -> Self {
        Self { inner }
    }
}

impl<T> From<BpTreeSet<T>> for BpTreeMap<T, ()> {
    #[inline]
    fn from(set: BpTreeSet<T>) -> Self {
        set.inner
    }
}

impl<T: Ord> FromIterator<T> for BpTreeSet<T> {
    #[inline]
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        let inner = BpTreeMap::<T, ()>::from_iter(iter.into_iter().map(|k| (k, ())));
        Self { inner }
    }
}

impl<T> IntoIterator for BpTreeSet<T> {
    type Item = T;
    type IntoIter = IterOwned<T>;

    #[inline]
    fn into_iter(self) -> Self::IntoIter {
        let inner = self.inner.into_iter();
        IterOwned { inner }
    }
}

impl<'a, T> IntoIterator for &'a BpTreeSet<T> {
    type Item = &'a T;
    type IntoIter = Iter<'a, T>;

    #[inline]
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

pub struct IterOwned<T> {
    inner: super::IterOwned<T, ()>,
}

impl<T> Iterator for IterOwned<T> {
    type Item = T;
    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(|(k, _)| k)
    }
}

pub struct Iter<'a, T> {
    inner: super::Iter<'a, T, ()>,
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;
    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(|(k, _)| k)
    }
}

use std::fmt::{Debug, Formatter as Fmt};

impl<T: Debug> Debug for BpTreeSet<T> {
    fn fmt(&self, f: &mut Fmt<'_>) -> std::fmt::Result {
        f.debug_list().entries(self.iter()).finish()
    }
}
