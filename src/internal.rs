use crate::{constant::*, node::*, utils};
use std::{mem::MaybeUninit, ptr::NonNull};

mod insert;
mod remove;

mod key_ref {
    #[derive(PartialEq, Eq)]
    pub(crate) struct KeyRef<K>(pub NonNull<K>);

    impl<K> Clone for KeyRef<K> {
        fn clone(&self) -> Self {
            Self(self.0)
        }
    }
    impl<K> Copy for KeyRef<K> {}

    impl<K> From<&K> for KeyRef<K> {
        fn from(k: &K) -> Self {
            Self(NonNull::from(k))
        }
    }

    impl<K: PartialOrd> std::cmp::PartialOrd<Self> for KeyRef<K> {
        fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
            unsafe { self.0.as_ref().partial_cmp(other.0.as_ref()) }
        }
    }

    impl<K: Ord> std::cmp::Ord for KeyRef<K> {
        fn cmp(&self, other: &Self) -> std::cmp::Ordering {
            unsafe { self.0.as_ref().cmp(other.0.as_ref()) }
        }
    }

    use std::{fmt::Debug, ptr::NonNull};
    impl<K: Debug> Debug for KeyRef<K> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            unsafe { self.0.as_ref().fmt(f) }
        }
    }
}

pub(crate) use key_ref::KeyRef;

pub(super) struct InternalNode<K, V> {
    parent: Option<NonNull<InternalNode<K, V>>>,
    nth: MaybeUninit<usize>,
    keys: MaybeUninit<[KeyRef<K>; CAPACITY]>,
    children: MaybeUninit<[AnyNode<K, V>; B]>,
    len: usize,
}

impl<K, V> TreeNode<K, V> for InternalNode<K, V> {
    #[inline]
    fn parent(&self) -> &Option<NonNull<InternalNode<K, V>>> {
        &self.parent
    }

    #[inline]
    fn parent_mut(&mut self) -> &mut Option<NonNull<InternalNode<K, V>>> {
        &mut self.parent
    }

    #[inline]
    fn nth(&self) -> &MaybeUninit<usize> {
        &self.nth
    }

    #[inline]
    fn nth_mut(&mut self) -> &mut MaybeUninit<usize> {
        &mut self.nth
    }

    #[inline]
    fn any(&mut self) -> AnyNode<K, V> {
        AnyNode::<K, V> {
            internal: NonNull::from(self),
        }
    }
}

impl<K, V> FromAnyNode<K, V> for InternalNode<K, V> {
    unsafe fn from_any(any: AnyNode<K, V>) -> NonNull<Self> {
        any.internal
    }
}

impl<K, V> InternalNode<K, V> {
    pub unsafe fn new_ptr_with_child(child: AnyNode<K, V>) -> NonNull<Self> {
        let mut ret: NonNull<Self> = utils::allocate();
        ret.as_mut().parent = None;
        ret.as_mut().len = 0;
        utils::insert_unchecked(ret.as_mut().children.assume_init_mut(), 0, 0, child);
        ret
    }

    #[inline]
    pub fn keys(&self) -> &[KeyRef<K>] {
        unsafe { &self.keys.assume_init_ref()[0..self.len] }
    }

    #[inline]
    pub fn keys_mut(&mut self) -> &mut [KeyRef<K>] {
        unsafe { &mut self.keys.assume_init_mut()[0..self.len] }
    }

    #[inline]
    pub fn children(&self) -> &[AnyNode<K, V>] {
        unsafe { &self.children.assume_init_ref()[0..self.len + 1] }
    }

    #[inline]
    pub fn children_mut(&mut self) -> &mut [AnyNode<K, V>] {
        unsafe { &mut self.children.assume_init_mut()[0..self.len + 1] }
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.len
    }
}

impl<K: Ord, V> InternalNode<K, V> {
    #[inline]
    pub fn get_child_by(&self, k: &K) -> AnyNode<K, V> {
        unsafe {
            *self.children().get_unchecked(
                match self.keys().binary_search(&KeyRef(NonNull::from(k))) {
                    Ok(idx) => idx + 1,
                    Err(idx) => idx,
                },
            )
        }
    }
}

impl<K, V> InternalNode<K, V> {
    #[inline]
    unsafe fn take_first_unchecked(&mut self) -> (KeyRef<K>, AnyNode<K, V>) {
        use utils::remove_unchecked as rmv;
        let k = rmv(self.keys.assume_init_mut(), self.len, 0);
        let child = rmv(self.children.assume_init_mut(), self.len + 1, 0);
        self.len -= 1;
        (k, child)
    }

    #[inline]
    unsafe fn insert_first_unchecked(&mut self, k: KeyRef<K>, child: AnyNode<K, V>) {
        use utils::insert_unchecked as ins;
        ins(self.keys.assume_init_mut(), self.len, 0, k);
        let children = self.children.assume_init_mut();
        ins(children, self.len + 1, 0, child);
        self.len += 1;
    }

    #[inline]
    unsafe fn insert_unchecked(&mut self, idx: usize, k: KeyRef<K>, child: AnyNode<K, V>) {
        use utils::insert_unchecked as ins;
        ins(self.keys.assume_init_mut(), self.len, idx, k);
        let children = self.children.assume_init_mut();
        ins(children, self.len + 1, idx + 1, child);
        self.len += 1;
    }

    #[inline]
    unsafe fn remove_unchecked(&mut self, idx: usize) -> (KeyRef<K>, AnyNode<K, V>) {
        use utils::remove_unchecked as rmv;
        let k = rmv(self.keys.assume_init_mut(), self.len, idx);
        let child = rmv(self.children.assume_init_mut(), self.len + 1, idx + 1);
        self.len -= 1;
        (k, child)
    }

    #[inline]
    unsafe fn pop_unchecked(&mut self) -> (KeyRef<K>, AnyNode<K, V>) {
        self.remove_unchecked(self.len - 1)
    }

    #[inline]
    unsafe fn push_unchecked(&mut self, k: KeyRef<K>, child: AnyNode<K, V>) {
        self.insert_unchecked(self.len, k, child)
    }

    fn update_childrens_parent_from_index<T: FromAnyNode<K, V>>(
        mut pself: NonNull<Self>,
        index: usize,
    ) {
        unsafe {
            let this = pself.as_mut();
            let children = this.children.assume_init_mut();
            for i in index..this.len + 1 {
                let child = *children.get_unchecked_mut(i);
                *T::from_any(child).as_mut().parent_mut() = Some(pself);
            }
        }
    }

    fn update_childrens_nth_from_index<T: FromAnyNode<K, V>>(&mut self, index: usize) {
        unsafe {
            let children = self.children.assume_init_mut();
            for i in index..self.len + 1 {
                let child = *children.get_unchecked_mut(i);
                *T::from_any(child).as_mut().nth_mut().assume_init_mut() = i;
            }
        }
    }
}
