use std::alloc::{alloc, Layout};
use std::ptr::NonNull;

#[inline]
pub(super) unsafe fn allocate<T>() -> NonNull<T> {
    let layout = Layout::new::<T>();
    let ptr = alloc(layout) as *mut T;
    NonNull::new_unchecked(ptr)
}

#[inline]
pub(super) unsafe fn insert_unchecked<T>(arr: &mut [T], len: usize, idx: usize, elem: T) {
    let p = arr.as_mut_ptr().add(idx);
    std::ptr::copy(p, p.offset(1), len - idx);
    std::ptr::write(p, elem);
}

#[inline]
pub(super) unsafe fn remove_unchecked<T>(arr: &mut [T], len: usize, idx: usize) -> T {
    let ptr = arr.as_mut_ptr().add(idx);
    let ret = std::ptr::read(ptr);
    std::ptr::copy(ptr.offset(1), ptr, len - idx - 1);
    ret
}
