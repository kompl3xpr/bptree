use crate::{constant::*, node::*, utils};
use std::{mem::MaybeUninit, ptr::NonNull};

mod insert;
mod remove;

pub(super) struct LeafNode<K, V> {
    keys: MaybeUninit<[K; CAPACITY]>,
    vals: MaybeUninit<[V; CAPACITY]>,
    parent: Option<NonNull<InternalNode<K, V>>>,
    nth: MaybeUninit<usize>,
    len: usize,
    pub(super) next: Option<NonNull<LeafNode<K, V>>>,
}

impl<K, V> TreeNode<K, V> for LeafNode<K, V> {
    #[inline]
    fn parent(&self) -> &Option<NonNull<InternalNode<K, V>>> {
        &self.parent
    }
    #[inline]
    fn parent_mut(&mut self) -> &mut Option<NonNull<InternalNode<K, V>>> {
        &mut self.parent
    }
    #[inline]
    fn nth(&self) -> &MaybeUninit<usize> {
        &self.nth
    }
    #[inline]
    fn nth_mut(&mut self) -> &mut MaybeUninit<usize> {
        &mut self.nth
    }
    #[inline]
    fn any(&mut self) -> AnyNode<K, V> {
        AnyNode::<K, V> {
            leaf: NonNull::from(self),
        }
    }
}

impl<K, V> FromAnyNode<K, V> for LeafNode<K, V> {
    unsafe fn from_any(any: AnyNode<K, V>) -> NonNull<Self> {
        any.leaf
    }
}

impl<K, V> std::ops::Drop for LeafNode<K, V> {
    fn drop(&mut self) {
        unsafe {
            for _ in 0..self.len {
                self.pop_unchecked();
            }
        }
    }
}

impl<K, V> LeafNode<K, V> {
    #[inline]
    pub fn next(&self) -> Option<NonNull<LeafNode<K, V>>> {
        self.next
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.len
    }

    #[inline]
    pub unsafe fn set_len(&mut self, len: usize) {
        self.len = len;
    }

    #[inline]
    pub fn pairs(&self) -> (&[K], &[V]) {
        let k = unsafe { &self.keys.assume_init_ref()[0..self.len] };
        let v = unsafe { &self.vals.assume_init_ref()[0..self.len] };
        (k, v)
    }

    #[inline]
    pub fn pairs_mut(&mut self) -> (&mut [K], &mut [V]) {
        let k = unsafe { &mut self.keys.assume_init_mut()[0..self.len] };
        let v = unsafe { &mut self.vals.assume_init_mut()[0..self.len] };
        (k, v)
    }

    #[inline]
    pub fn keys(&self) -> &[K] {
        unsafe { &self.keys.assume_init_ref()[0..self.len] }
    }

    pub unsafe fn new_ptr() -> NonNull<Self> {
        use std::ptr::write;
        let mut ret: NonNull<Self> = utils::allocate();
        write(&mut ret.as_mut().parent as *mut _, None);
        write(&mut ret.as_mut().next as *mut _, None);
        write(&mut ret.as_mut().len as *mut _, 0_usize);
        ret
    }
}
impl<K, V> LeafNode<K, V> {
    #[inline]
    pub unsafe fn insert_unchecked(&mut self, idx: usize, k: K, v: V) {
        use utils::insert_unchecked as ins;
        ins(self.keys.assume_init_mut(), self.len, idx, k);
        ins(self.vals.assume_init_mut(), self.len, idx, v);
        self.len += 1;
    }

    #[inline]
    pub unsafe fn remove_unchecked(&mut self, idx: usize) -> (K, V) {
        use utils::remove_unchecked as rmv;
        let k = rmv(self.keys.assume_init_mut(), self.len, idx);
        let v = rmv(self.vals.assume_init_mut(), self.len, idx);
        self.len -= 1;
        (k, v)
    }

    #[inline]
    pub unsafe fn pop_unchecked(&mut self) -> (K, V) {
        self.remove_unchecked(self.len - 1)
    }

    #[inline]
    pub unsafe fn push_unchecked(&mut self, k: K, v: V) {
        self.insert_unchecked(self.len(), k, v)
    }
}

impl<K: Ord, V> LeafNode<K, V> {
    #[inline]
    pub fn get_by(&self, k: &K) -> Option<&V> {
        unsafe {
            let (keys, values) = self.pairs();
            keys.binary_search(k)
                .ok()
                .map(|idx| values.get_unchecked(idx))
        }
    }

    #[inline]
    pub fn get_mut_by(&mut self, k: &K) -> Option<&mut V> {
        unsafe {
            let (keys, values) = self.pairs_mut();
            keys.binary_search(k)
                .ok()
                .map(|idx| values.get_unchecked_mut(idx))
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn drop_check() {
        use std::{cell::RefCell, rc::Rc};
        struct MyKey(Rc<RefCell<usize>>);
        impl std::ops::Drop for MyKey {
            fn drop(&mut self) {
                *self.0.borrow_mut() += 1;
            }
        }

        unsafe {
            for len in 0..CAPACITY {
                let counter = Rc::new(RefCell::new(0));
                let mut leaf = LeafNode::new_ptr();
                for i in 0..len {
                    leaf.as_mut()
                        .insert_unchecked(i, MyKey(counter.clone()), ());
                }
                Box::from_raw(leaf.as_ptr());
                assert_eq!(*counter.borrow(), len);
            }
        }
    }
}
