use crate::{constant::*, node::*};
use std::ptr::NonNull;

impl<K: Ord, V> LeafNode<K, V> {
    pub fn remove(this: NonNull<Self>, key: &K) -> Option<((K, V), MaybeAnyNode<K, V>)> {
        unsafe {
            let index = this.as_ref().keys().binary_search(&key).ok()?;
            Some(Self::remove_by_index_unchecked(this, index))
        }
    }

    pub fn remove_last(this: NonNull<Self>) -> Option<((K, V), MaybeAnyNode<K, V>)> {
        unsafe {
            let len = this.as_ref().len();
            len.checked_sub(1)
                .map(|idx| Self::remove_by_index_unchecked(this, idx))
        }
    }

    pub fn remove_first(this: NonNull<Self>) -> Option<((K, V), MaybeAnyNode<K, V>)> {
        unsafe {
            let len = this.as_ref().len();
            len.checked_sub(1)
                .map(|_| Self::remove_by_index_unchecked(this, 0))
        }
    }

    unsafe fn remove_by_index_unchecked(
        mut this: NonNull<Self>,
        index: usize,
    ) -> ((K, V), MaybeAnyNode<K, V>) {
        let leaf = this.as_mut();
        let ret = leaf.remove_unchecked(index);
        if leaf.len() > (CAPACITY - 1) / 2 {
            return (ret, MaybeAnyNode::none());
        }
        let parent = match leaf.parent {
            Some(parent) => parent,
            None => return (ret, MaybeAnyNode::none()),
        };
        let idx = leaf.nth().assume_init();
        let root = InternalNode::<K, V>::remove_leaf(parent, idx);
        (ret, root)
    }
}
