use crate::{constant::*, node::*};
use std::ptr::NonNull;

impl<K: Ord, V> LeafNode<K, V> {
    unsafe fn insert_and_split(&mut self, idx: usize, k: K, v: V) -> NonNull<Self> {
        let mut other = Self::new_ptr();
        let insert_left = idx <= HALF_CAP;
        let move_cnt = B / 2 - if insert_left { 0 } else { 1 };
        for _ in 0..move_cnt {
            let (k, v) = self.pop_unchecked();
            other.as_mut().insert_unchecked(0, k, v);
        }
        match insert_left {
            true => self.insert_unchecked(idx, k, v),
            false => other.as_mut().insert_unchecked(idx - (HALF_CAP + 1), k, v),
        };
        other.as_mut().next = self.next;
        self.next = Some(other);
        other
    }

    pub fn insert(&mut self, k: K, v: V) -> Option<V> {
        unsafe {
            let mut pself = NonNull::from(self);
            let leaf = pself.as_mut();
            let (keys, vals) = leaf.pairs_mut();
            let idx = match keys.binary_search(&k) {
                Ok(i) => return Some(std::mem::replace(vals.get_unchecked_mut(i), v)),
                Err(i) => i,
            };
            if leaf.len < CAPACITY {
                leaf.insert_unchecked(idx, k, v);
                return None;
            }
            let mut other = leaf.insert_and_split(idx, k, v);
            let (mut parent, idx) = match leaf.parent {
                None => {
                    leaf.nth.write(0);
                    let parent = InternalNode::new_ptr_with_child(AnyNode { leaf: pself });
                    leaf.parent = Some(parent);
                    (parent, 0)
                }
                Some(parent) => (parent, leaf.nth.assume_init()),
            };

            other.as_mut().nth_mut().write(0);
            other.as_mut().parent = Some(parent);
            let key = KeyRef::from(other.as_ref().keys().get_unchecked(0));
            parent
                .as_mut()
                .insert::<LeafNode<K, V>>(idx, key, other.as_mut().any());
            None
        }
    }
}
